package co.com.tarea.service;

import co.com.tarea.dto.VentaDTO;
import co.com.tarea.model.Venta;

public interface IVentaService extends ICRUD<Venta, Integer> {

	Venta grabarDetalleVenta(VentaDTO dto) throws Exception;
}
