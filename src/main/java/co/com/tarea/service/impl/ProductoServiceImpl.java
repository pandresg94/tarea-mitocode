package co.com.tarea.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tarea.model.Producto;
import co.com.tarea.repo.IGenericRepo;
import co.com.tarea.repo.IProductoRepo;
import co.com.tarea.service.IProductoService;

@Service
public class ProductoServiceImpl extends CRUDImpl<Producto, Integer> implements IProductoService {

	@Autowired
	IProductoRepo repo;

	@Override
	public IGenericRepo<Producto, Integer> getRepo() {
		return repo;
	}

}
