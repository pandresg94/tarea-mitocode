package co.com.tarea.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tarea.dto.VentaDTO;
import co.com.tarea.model.Venta;
import co.com.tarea.repo.IGenericRepo;
import co.com.tarea.repo.IVentaRepo;
import co.com.tarea.service.IVentaService;

@Service
public class VentaServiceImpl extends CRUDImpl<Venta, Integer> implements IVentaService {

	@Autowired
	IVentaRepo repo;

	@Autowired

	@Override
	public IGenericRepo<Venta, Integer> getRepo() {
		return repo;
	}

	@Transactional
	@Override
	public Venta grabarDetalleVenta(VentaDTO dto) throws Exception {

		dto.getVenta().getDetalleVenta().forEach(x -> x.setVenta(dto.getVenta()));

		return repo.save(dto.getVenta());

	}

}
