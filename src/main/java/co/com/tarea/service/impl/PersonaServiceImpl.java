package co.com.tarea.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.tarea.model.Persona;
import co.com.tarea.repo.IGenericRepo;
import co.com.tarea.repo.IPersonaRepo;
import co.com.tarea.service.IPersonaService;

@Service
public class PersonaServiceImpl extends CRUDImpl<Persona, Integer> implements IPersonaService {

	@Autowired
	IPersonaRepo repo;

	@Override
	public IGenericRepo<Persona, Integer> getRepo() {
		return repo;
	}

}
