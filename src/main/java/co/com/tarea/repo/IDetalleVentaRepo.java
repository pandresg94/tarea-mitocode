package co.com.tarea.repo;

import co.com.tarea.model.DetalleVenta;

public interface IDetalleVentaRepo extends IGenericRepo<DetalleVenta, Integer> {

}
