package co.com.tarea.repo;

import co.com.tarea.model.Venta;

public interface IVentaRepo extends IGenericRepo<Venta, Integer> {

}
