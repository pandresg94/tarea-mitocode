package co.com.tarea.repo;

import co.com.tarea.model.Persona;

public interface IPersonaRepo extends IGenericRepo<Persona, Integer> {

}
