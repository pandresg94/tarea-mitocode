package co.com.tarea.repo;

import co.com.tarea.model.Producto;

public interface IProductoRepo extends IGenericRepo<Producto, Integer> {

}
