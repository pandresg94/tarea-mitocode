package co.com.tarea.exception;

public class ModeloNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 598775676017353367L;

	public ModeloNotFoundException(String mensaje) {
		super(mensaje);
	}

}
