package co.com.tarea.dto;

import co.com.tarea.model.Venta;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VentaDTO {

	private Venta venta;

}
