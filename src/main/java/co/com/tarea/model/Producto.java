package co.com.tarea.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tbl_producto")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_producto")
	private Integer idProducto;
	@Column(name = "nombre", nullable = false, length = 60)
	@Size(min = 3, message = "El campo nombre debe tener minimo 3 digitos")
	@NotNull(message = "El campo nombre no puede ser nulo")
	private String nombre;
	@Column(name = "marca", nullable = false, length = 60)
	@Size(min = 3, message = "El campo marca debe tener minimo 3 digitos")
	@NotNull(message = "El campo marca no puede ser nulo")
	private String marca;
}
