package co.com.tarea.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tbl_persona")
public class Persona {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_persona")
	private Integer idPersona;
	@Column(name = "nombres", nullable = false, length = 60)
	@Size(min = 3, message = "{nombres.size}")
	@NotNull(message = "El campo nombres no puede ser nulo")
	private String nombres;
	@Column(name = "apellidos", nullable = false, length = 60)
	@Size(min = 3, message = "El campo apellidos debe tener minimo 3 digitos")
	@NotNull(message = "El campo apellidos no puede ser nulo")
	private String apellidos;
}
