package co.com.tarea.controller;

import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.tarea.exception.ModeloNotFoundException;
import co.com.tarea.model.Producto;
import co.com.tarea.service.IProductoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/v1/Productos")
public class ProductoController {

	@Autowired
	IProductoService service;

	@ApiOperation(value = "Operacion del servicio que retorna el listado de productos", notes = "la operacion consulta el listado de productos existentes y lo retorna")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "exitoso"),
			@ApiResponse(code = 400, message = "no existe informacion"),
			@ApiResponse(code = 500, message = "Error interno del servidor") })
	@GetMapping
	public ResponseEntity<List<Producto>> listar() throws Exception {
		List<Producto> obj = service.listar();

		if (obj.isEmpty())
			throw new ModeloNotFoundException("no existe informacion");

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@ApiOperation(value = "Operacion del servicio que retorna producto por id", notes = "la operacion consulta el producto por id y lo retorna")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "exitoso"),
			@ApiResponse(code = 400, message = "no existe informacion"),
			@ApiResponse(code = 500, message = "Error interno del servidor") })
	@GetMapping("/{id}")
	public ResponseEntity<Producto> listarPorId(@PathVariable("id") Integer id) throws Exception {
		Producto obj = service.listarPorId(id);

		if (Objects.isNull(obj))
			throw new ModeloNotFoundException("no existe informacion");

		return new ResponseEntity<>(obj, HttpStatus.OK);
	}

	@ApiOperation(value = "Operacion del servicio que graba un producto", notes = "la operacion graba en BD un producto")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "creado"),
			@ApiResponse(code = 500, message = "Error interno del servidor") })
	@PostMapping
	public ResponseEntity<Producto> guardar(@Valid @RequestBody Producto obj) throws Exception {

		Producto objSave = service.registrar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.CREATED);
	}

	@ApiOperation(value = "Operacion del servicio que actualiza un producto", notes = "la operacion actualiza en BD un producto")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "exitoso"),
			@ApiResponse(code = 500, message = "Error interno del servidor") })
	@PutMapping
	public ResponseEntity<Producto> actualizar(@Valid @RequestBody Producto obj) throws Exception {

		Producto objSave = service.registrar(obj);
		return new ResponseEntity<>(objSave, HttpStatus.OK);
	}

	@ApiOperation(value = "Operacion del servicio que elimina un producto", notes = "la operacion elimina en BD un producto")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "exitoso"),
			@ApiResponse(code = 500, message = "Error interno del servidor") })
	@PutMapping("/{id}")
	public ResponseEntity<Void> actualizar(@PathVariable("id") Integer id) throws Exception {

		service.eliminar(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
